#include "mbed.h"

/*
 * Utilisation du PWM
 * On règle la fréquence du PWM en fonction de la valeur du Potentiomètre 
 * Utilisation du Pin D5 pour la LED
 * Utilisation du Pin A0 pour le potentiomètre
 */

int main()
{
    PwmOut output(D5);
    AnalogIn input(A0);

    output.period_ms(4000);
    output = 0.5; // Rapport cyclique de 50% (autant de temps allumé que éteint)
    int pr = 0; // Représentation sous la forme entière du potentiomètre [O; 10001]
    while (true) {
        if (abs(pr - input.read() * 1000 + 1) > 5) // Si la valeur du potentiomètre est différente d'au moins 5 de la dernière version lue
        {
            pr = input.read() * 1000 + 1; // Sauvegarde d'une nouvelle version
            output.period_ms(pr); // Changement de la période (et donc de la fréquence)
            //printf("%d \n",pr); // Affichage de la version sur le port série si nécessaire
            wait_us(500000); // On ne vérifie pas en permanence la valeur. On attend 0.5 avant de chercher une update.
        }

    }
}
