// compiled with mbed-os 5
#include "mbed.h"
// the header of the "lib" we have to use
#include "DFPlayerMini.h"

// main() runs in its own thread in the OS
int main()
{
    DFPlayerMini a(D10, D2);
    // This will give a value between [0,1]:
    // 1 being the potentiometer in fully "opened" position
    // 0 in "closed" position
    AnalogIn     volumeCtrl(A0);
    DigitalIn    btnPrevious(D3);
    DigitalIn    btnPause(D4);
    DigitalIn    btnNext(D5);
    DigitalOut   DebugLed(LED1);

    float t = 0.f;
    uint16_t volumeValue = 0;
    bool IsOn = true;

    // let's stop the DFPlayer first, because it may be already singing
    a.mp3_stop();

    // relaunch the DFPlayer
    a.mp3_play();

    while (true) {
        // here we handle the various inputs :s
        if (btnPrevious) {
            a.mp3_prev();
            DebugLed = !DebugLed;
            while (btnPrevious);
        }  
        if (btnPause && IsOn) {
            a.mp3_pause();
            IsOn = !IsOn;
            DebugLed = !DebugLed;
            while (btnPause);
        }
        else if (btnPause && !IsOn) {
            a.mp3_play();
            IsOn = !IsOn;
            DebugLed = !DebugLed;
            while (btnPause);
        }
        if (btnNext) {
            a.mp3_next();
            while (btnNext);
        }

        // recall: t is a floating point value between [0,1]
        t = volumeCtrl.read();
        // as mp3_set_volume() takes an integer between 0 and 30,
        // we just have to multiply the floating point value by 30
        // and cast the resulting value into an integer
        volumeValue = static_cast<uint16_t>(t*30);

        // set the current volume with the aformentionned value
        a.mp3_set_volume(volumeValue);

        // then wait for 100ms
        wait_us(100000);
    }
}
